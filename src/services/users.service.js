const bcrypt = require('bcrypt');
const validator = require('validator');

const userRepo = require('../repository/users.mongo-repo');

class UsersServices {
  constructor() {
    this.userRepo = userRepo;
  }

  async createUser(userForm) {
    // validate form
    const error = {};
    const usernameRegex = /^[a-zA-Z0-9-.]+$/;
    if (!userForm.username || !usernameRegex.test(userForm.username)) {
      error['username'] = 'invalid username';
    }

    if (!userForm.email || !validator.isEmail(userForm.email)) {
      error['email'] = 'invalid email';
    }

    if (!userForm.password) {
      error['password'] = 'invalid password';
    }

    if (userForm.password !== userForm.confirmPassword) {
      error['confirmPassword'] = 'confirmPassword mismatch';
    }

    if (Object.keys(error).length !== 0) {
      throw error;
    }

    // hash password
    const hashedPassword = await bcrypt.hash(userForm.password, 10);
    delete userForm.password
    delete userForm.confirmPassword
    userForm.hashedPassword = hashedPassword

    // store to database
    const newUser = await this.userRepo.createUser(userForm);
    delete newUser.hashedPassword
    return newUser;
  }
}


const userService = new UsersServices();

module.exports = userService;
