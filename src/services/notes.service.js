const validator = require('validator');

const noteRepo = require('../repository/notes.mongo-repo');

class notesServices {
  constructor() {
    this.noteRepo = noteRepo;
  }

  async createNote(noteForm) {
    // validate form
    const error = {};
    const noteRegex = /^[a-zA-Z0-9-.]+$/;
    if (!noteForm.note || !noteRegex.test(noteForm.note)) {
      error['note'] = 'invalid note';
    }

    if (Object.keys(error).length !== 0) {
      throw error;
    }

  }
}

const noteService = new notesServices();

module.exports = noteService;
