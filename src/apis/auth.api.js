const path = require('path');

const utils = require('../utils');

/**
 * Handle login page using async/await
 * @param {Express.Request} req req instance
 * @param {Express.Response} res 
 */
const loginController = async (req, res) => {
  // find path of login template
  const indexFile = path.join(utils.rootPath, 'template', 'login.html');
  try {
    // read and wait for content of login.html
    const indexContent = await utils.readFile(indexFile);
    // response to client
    res.header('Content-Type', 'text/html');
    res.send(indexContent);
  } catch (error) {
    res.json({ error: error });
  }
};

/**
 * Handle register path using promise 
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 */
const registerController = (req, res) => {
  const registerHtml = path.join(utils.rootPath, 'template', 'register.html');
  utils.readFile(registerHtml)
    // khi lời hứa được thực hiện
    .then((content) => {
      res.header('Content-Type', 'text/html');
      res.send(content);
    })
    // lời hứa bị xù 
    .catch((error) => {
      res.json({ error: error.message });
    })
};


// export to other modules
module.exports = {
  loginController,
  registerController,
};
