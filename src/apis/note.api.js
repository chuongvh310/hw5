const noteRepo = require('../repository/notes.mongo-repo');
const noteService = require('../services/notes.service');
const express = require('express');

const findAllNotesController = async (req, resp) => {
  const notes = await noteRepo.readNotes();
  resp.json(notes);
};

const createNoteController = async (req, resp) => {
  try {
    const noteForm = req.body;
    const noteInserted = await noteService.createNote(noteForm);
    resp.json(noteInserted);
  }
  catch (err) {
    resp.status(400).json({error : err});
    }
};

const updateNoteController = async (req, resp) => {
  const noteId = req.params.id;
 const isUpdated = await noteRepo.updatedById(noteId, req.body);
  // update note info
  resp.json({ 'update': 'ok' });
};

const deleteNoteController = async (req, resp) => {
  const noteId = req.params.id;
  const isDelete = await noteRepo.deleteById(noteId);
  console.log('delete noteId', noteId, isDelete)
  resp.sendStatus(204);
};

const findDetailNoteController = async (req, resp) => {
  const noteDetail = await noteRepo.findById(req.params.id);
  // find notes
  // noteDetail.notes = notes;
  resp.json(noteDetail);
};

const noteRoutes = express.Router();

noteRoutes.get('/', findAllNotesController);
noteRoutes.post('/', createNoteController);
noteRoutes.put('/:id', updateNoteController);
noteRoutes.delete('/:id', deleteNoteController);
noteRoutes.get('/:id', findDetailNoteController);

module.exports = {
  noteRoutes,
};