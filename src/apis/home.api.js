const fs = require('fs');
const path = require('path');

const utils = require('../utils');
/**
 * Handle home page
 * @param {Express.Request} req 
 * @param {Express.Response} res 
 */
const homepageController = (req, res) => {
  const indexFile = path.join(utils.rootPath, 'template', 'index.html');
  // easy solution
  // res.sendFile(indexFile);
  // sử dụng callback
  fs.readFile(indexFile, (err, content) => {
    if (err) {
      res.json({ error: error });
    } else {
      res.header('Content-Type', 'text/html');
      res.send(content);
    }
  })
};

module.exports = {
  homepageController,
};
