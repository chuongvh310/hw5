const userRepo = require('../repository/users.mongo-repo');
const userService = require('../services/users.service');
const express = require('express');


const findAllUsersController = async (req, resp) => {
  const users = await userRepo.allUsers();
  resp.json(users);
};

const createUserController = async (req, resp) => {
  try {
    const userForm = req.body;
    const userInserted = await userService.createUser(userForm);
    resp.json(userInserted);
  }
  catch (err) {
    resp.status(400).json({error : err});
    }
};

const updateUserController = async (req, resp) => {
  const userId = req.params.id;
 const isUpdated = await userRepo.updatedById(userId, req.body);
  // update user info
  resp.json({ 'update': 'ok' });
};

/**
 * 
 * @param {Express.Request} req 
 * @param {Express.Response} resp 
 */
const deleteUserController = async (req, resp) => {
  const userId = req.params.id;
  const isDelete = await userRepo.deleteById(userId);
  console.log('delete userId', userId, isDelete)
  resp.sendStatus(204);
};

const findDetailUserController = async (req, resp) => {
  const userDetail = await userRepo.findById(req.params.id);
  // find notes
  // userDetail.notes = notes;
  resp.json(userDetail);
};
const findNotesOfUserController = async (req, resp) => {
  const userDetail = await userRepo.findById(req.params.id);
  resp.json(userDetail)
}
const userRoutes = express.Router();

userRoutes.get('/', findAllUsersController);
userRoutes.post('/', createUserController);
userRoutes.put('/:id', updateUserController);
userRoutes.delete('/:id', deleteUserController);
userRoutes.get('/:id', findDetailUserController);
userRoutes.get('/:id/notes', findNotesOfUserController)

module.exports = {
  userRoutes,
};