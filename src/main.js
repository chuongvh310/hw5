
const path  = require('path');
const express = require('express');
const bodyParser = require('body-parser');

const mongodb = require('./config/database');
const utils = require('./utils');
const homeApis = require('./apis/home.api');
const authApis = require('./apis/auth.api');
const userApis = require('./apis/user.api');
const noteApis = require('./apis/note.api');

const main = async () => {
await mongodb.connect();
const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 
app.set("view options", { layout: false });
// server chay file css, js
// middleware
app.use('/public', express.static(path.join(utils.rootPath, 'public')));

app.use('/users', userApis.userRoutes);
app.use('/notes', noteApis.noteRoutes);

const port = +process.env.PORT || 6060;

app.listen(port, (error) => {
  if (error) {
    return console.error('run server got an error', error);
  }
  return console.log(`Server listening ${port}`);
});
};

main();
