const path = require('path');
const { ObjectId } = require('mongodb');
const mongodb = require('../config/database');
const utils = require ('../utils');

class UserMongoRepo {
  constructor() {
    this.mongodb = mongodb;
  }

  userCollection() {
    return this.mongodb.getDb().collection('users');
  }
  
  async allUsers() {
    const users = await this.userCollection().find({}).toArray();
    console.log(users);
    return users;
  }

  async findById(userId) {
    if(!ObjectId.isValid(userId)) {
      throw new TypeError ('Invalid userId');
    }
    const user = await this.userCollection().findOne({
      _id: ObjectId(userId),
    });
   
    return user;
  }

  async createUser(userForm) {
    const insertResult = await this.userCollection().insertOne(userForm);
    userForm.id = insertResult.insertedId;
    return userForm;
  }

  async deleteById(userId) {
    const deleteResult = await this.userCollection().deleteOne({
      _id: ObjectId(userId),
    });
    if(deleteResult.deletedCount === 1)
      return true;
    else 
    return false;
  }

  async updatedById(userId, modifier) {
    const updatedResult = await this.userCollection().updateOne(
      { _id: ObjectId(userId) },
      {$set : modifier},
    );
    console.log(updatedResult);
    return true;
  }
}
const userRepo = new UserMongoRepo;
module.exports =userRepo;