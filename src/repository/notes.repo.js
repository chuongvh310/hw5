const path = require('path');

const utils = require('../utils');

const readNotes = async () => {
  const notesJsonPath = path.join(utils.rootPath, 'data', 'notes.json');
  const notesContent = await utils.readFile(notesJsonPath);
  const notesJson = JSON.parse(notesContent);
  return notesJson;
 };
const findById = async (noteId) => {
  const notes = await readNotes();
  const noteDetail = notes.find(note => note.id === noteId);
  return noteDetail;
};
module.exports = {
  readNotes,
  findById,
};