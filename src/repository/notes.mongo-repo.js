const path = require('path');
const { ObjectId } = require('mongodb');
const mongodb = require('../config/database');
const utils = require ('../utils');

class noteMongoRepo {
  constructor() {
    this.mongodb = mongodb;
  }

  noteCollection() {
    return this.mongodb.getDb().collection('notes');
  }
  
  async readNotes() {
    const notes = await this.noteCollection().find({}).toArray();
    console.log(notes);
    return notes;
  }

  async findById(noteId) {
    if(!ObjectId.isValid(noteId)) {
      throw new TypeError ('Invalid noteId');
    }
    const note = await this.noteCollection().findOne(
      { _id: ObjectId(noteId) },
    
    );
  
    return note;
  }

  async createNote(noteForm) {
    const insertResult = await this.noteCollection().insertOne(noteForm);
    noteForm.id = insertResult.insertedId;
    return noteForm;
  }

  async deleteById(noteId) {
    const deleteResult = await this.noteCollection().deleteOne({
      _id: ObjectId(noteId),
    });
    if(deleteResult.deletedCount === 1)
      return true;
    else 
    return false;
  }

  async updatedById(noteId, modifier) {
    const updatedResult = await this.noteCollection().updateOne(
      { _id: ObjectId(noteId) },
      {$set : modifier},
    );
    console.log(updatedResult);
    return true;
  }
}
const noteRepo = new noteMongoRepo;
module.exports = noteRepo;